package com.ownit.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Hello world!
 *
 */


@SpringBootApplication
@ComponentScan(basePackages= {"com.ownit","com.ownit.dataaccess"})
@EnableJpaRepositories(basePackages="com.ownit.dataaccess.repository")
@EntityScan("com.ownit.dataaccess.model")
@PropertySource({ "classpath:application.properties", "classpath:message.properties" })
public class Application 
{
    public static void main( String[] args )
    {
       SpringApplication.run(Application.class, args);
    }
}
