package com.ownit.admin.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ownit.common.dto.ProductInfoDTO;
import com.ownit.common.dto.StatusResponseDTO;
import com.ownit.common.service.ProductInfoService;
import com.ownit.common.utils.RegistrationUtils;
import com.ownit.dataaccess.model.ProductInfo;
import com.ownit.dataaccess.model.UserInfo;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value="admin/product") 
@CrossOrigin
public class ProductController {
	
	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductInfoService productInfoService;
	
	@Autowired
	Environment env;
	
	@PostMapping(value = "/saveproductinfo", produces = "application/json")
	@ApiOperation(value = "Product Info", notes = "Add Product Info Details")
	public ResponseEntity<String> productInfo(
			@ApiParam(value = "Product Profile information") @RequestParam("imageFile") MultipartFile imageFile,
			@RequestParam(name = "commonProductDetails", value = "commonProductDetails") String commonProductDetails) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		ProductInfoDTO productInfo = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			productInfo = mapper.readValue(commonProductDetails, ProductInfoDTO.class);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("validate.register.faild"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		try {
			boolean inputValidation = RegistrationUtils.productInfoInputValidation(productInfo);
			if (!inputValidation) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.product.input"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isSaved = productInfoService.saveProductInfo(productInfo, imageFile);
			if (isSaved) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("productinfo.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("productinfo.failure"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}
	
	
	@GetMapping(value = "/productlist", produces = "application/json")
	public ResponseEntity<String> productList() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {

			List<ProductInfo> productInfo = productInfoService.productList();
			if (productInfo != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("productlist.success"));
				statusResponseDTO.setProductListData(productInfo);

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("productlist.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Problem in registration  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
