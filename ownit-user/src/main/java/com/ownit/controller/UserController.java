package com.ownit.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ownit.common.controller.RegistrationController;
import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.ProductInfoDTO;
import com.ownit.common.dto.StatusResponseDTO;
import com.ownit.common.service.ProductInfoService;
import com.ownit.common.service.UserRegistrationService;
import com.ownit.common.service.UserService;
import com.ownit.common.utils.RegistrationUtils;
import com.ownit.dataaccess.model.UserInfo;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("Api/User")
@CrossOrigin
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	Environment env;

	@Autowired
	UserRegistrationService userRegistrationService;

	@Autowired
	UserService userService;

	@Autowired
	ProductInfoService productInfoService;

	@PostMapping(value = "/Profile", produces = "application/json")
	@ApiOperation(value = "User account", notes = "Update user details")
	public ResponseEntity<String> userProfile(
			@ApiParam(value = "User Profile information") @RequestParam(name = "commonUserDetails", value = "commonUserDetails") String commonUserDetails) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		CommonUserDetails commonUserInfo = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			commonUserInfo = mapper.readValue(commonUserDetails, CommonUserDetails.class);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("validate.register.faild"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		try {
			// validate input fields
			boolean inputValidation = RegistrationUtils.inputValidation(commonUserInfo);
			if (!inputValidation) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.input"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isuserInfo = userRegistrationService.userProfile(commonUserInfo);
			if (isuserInfo) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("update.profile.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("update.profile.failed"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	@PostMapping(value = "/imgFilePath", produces = "application/json")
	public ResponseEntity<String> saveimg(@RequestParam("imageFile") MultipartFile imageFile) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {
			boolean file = userRegistrationService.savePicture(imageFile);
			if (file) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("image.file.upload"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("image.file.failure"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/profileImage", produces = "application/json")
	public ResponseEntity<String> profileimg(@RequestParam("imageFile") MultipartFile imageFile,
			@RequestParam(name = "commonUserDetails", value = "commonUserDetails") String commonUserDetails) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		CommonUserDetails commonUserInfo = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			commonUserInfo = mapper.readValue(commonUserDetails, CommonUserDetails.class);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("validate.register.faild"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
		try {
			boolean data = userRegistrationService.saveProfileImg(commonUserInfo, imageFile);
			
			if (data) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("image.file.upload"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("image.file.failure"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



}
