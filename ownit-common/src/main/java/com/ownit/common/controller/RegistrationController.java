package com.ownit.common.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.SocialSitesDTO;
import com.ownit.common.dto.StatusResponseDTO;
import com.ownit.common.dto.UpdateLoginDetailsDTO;
import com.ownit.common.service.UserRegistrationService;
import com.ownit.common.utils.EmailNotificationUtils;
import com.ownit.common.utils.RegistrationUtils;
import com.ownit.dataaccess.model.LoginInfo;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.repository.LogInfoRepository;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("Api/Register")
@CrossOrigin
public class RegistrationController {

	private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	Environment env;

	@Autowired
	UserRegistrationService userRegistrationService;

	@Autowired
	EmailNotificationUtils emailNotificationUtils;

	@Autowired
	RegistrationUtils registrationUtils;

	@Autowired
	LogInfoRepository loginInfoRepository;

	@CrossOrigin
	@PostMapping(value = "/userRegistration", produces = "application/json")
	@ApiOperation(value = "register account", notes = "Insert user details")
	public ResponseEntity<String> userRegistration(
			@ApiParam(value = "Required user information") @RequestParam(name = "commonUserDetails", value = "commonUserDetails") String commonUserDetails) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		CommonUserDetails commonUserInfo = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			commonUserInfo = mapper.readValue(commonUserDetails, CommonUserDetails.class);

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception :" + e.getMessage());
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("validate.register.faild"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		try {
			// validate input fields
			boolean inputValidation = RegistrationUtils.inputValidation(commonUserInfo);
			if (!inputValidation) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.input"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			// validating email here
			boolean emailValidate = RegistrationUtils.validateEmail(commonUserInfo.getEmail());
			if (!emailValidate) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.email"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			// existing user
			List<UserInfo> emailIdExist = userRegistrationService.checkEmailExist(commonUserInfo.getEmail());
			if (emailIdExist.size() > 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			// final save here
			String status = userRegistrationService.saveUser(commonUserInfo);
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("register.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	@CrossOrigin
	@PostMapping(value = "/createNewPassword", produces = "application/json")
	@ApiOperation(value = "forgot ", notes = "check forgot pwd")
	public synchronized ResponseEntity<String> forgotPassword(
			@ApiParam(value = "Required user information") @RequestBody UpdateLoginDetailsDTO updateLoginDetailsDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			boolean isValidEmailId = RegistrationUtils.validateLoginInfo(updateLoginDetailsDTO);

			if (!isValidEmailId) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.email"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			LoginInfo loginInfo = loginInfoRepository.findByEmail(updateLoginDetailsDTO.getUserName());
			boolean randomPasswordEqual = RegistrationUtils.checkPassword(updateLoginDetailsDTO.getTempPassword(),
					loginInfo.getRandomPassword());

			if (!randomPasswordEqual) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("random.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			if (!updateLoginDetailsDTO.getPassword().equals(updateLoginDetailsDTO.getConfirmPassword())) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("random.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			boolean passwordStatus = userRegistrationService.changePassword(updateLoginDetailsDTO.getUserName(),
					updateLoginDetailsDTO.getConfirmPassword());

			if (passwordStatus == false) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("email.notexist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("password.update"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	@PostMapping(value = "/verify/{email}", produces = "application/json")
	@ApiOperation(value = "register verify", notes = "Change status")
	public ResponseEntity<String> registerVerify(@ApiParam(value = "Required user mailId") @PathVariable String email) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean isEmailExist = userRegistrationService.isEmailExist(email);
			if (!isEmailExist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("mailid.notexist.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			boolean regStatus = userRegistrationService.checkRegistrationStatus(email);
			if (!regStatus) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("register.status.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			String status = userRegistrationService.verifyUser(email);
			if (status.equalsIgnoreCase("Success")) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("verify.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("findmail.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}

	}

	@PostMapping(value = "/socialSites", produces = "application/json")
	public ResponseEntity<String> socialSites(
			@ApiParam() @RequestParam(value = "socialSiteDetails", name = "socialSiteDetails") String socialSiteDetails) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		log.info("Social Site Page");

		SocialSitesDTO socialSiteInfo = new SocialSitesDTO();
		try {

			ObjectMapper mapper = new ObjectMapper();
			socialSiteInfo = mapper.readValue(socialSiteDetails, SocialSitesDTO.class);

			log.info("starting info" + socialSiteInfo);
			log.info("socialSiteInfo=====>" + socialSiteInfo.getUserType());

			boolean inputValidate = RegistrationUtils.socialInputValidation(socialSiteInfo);
			if (!inputValidate) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("input.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isEmailExist = userRegistrationService.isEmailExistInUserLoginInfo(socialSiteInfo.getUserId());
			if (!isEmailExist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("social.userid.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean saveStatus = userRegistrationService.socialSiteSave(socialSiteInfo);
			if (!saveStatus) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("social.site.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("social.site.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(value = "/resetpassword", produces = "application/json")
	public ResponseEntity<String> resetPassword(
			@ApiParam() @RequestParam(value = "passwordDetails", name = "passwordDetails") String passwordDetails) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		CommonUserDetails commonUserDetails = new CommonUserDetails();
		try {

			ObjectMapper mapper = new ObjectMapper();
			commonUserDetails = mapper.readValue(passwordDetails, CommonUserDetails.class);

			boolean inputValidate = RegistrationUtils.resetPasswordInputValidation(commonUserDetails);
			if (!inputValidate) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("input.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isUserInfoExist = userRegistrationService.isLoginInfoExist(commonUserDetails.getUserInfoId());
			if (!isUserInfoExist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("userinfo.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isCurrentPasswordExist = userRegistrationService.isPasswordExist(commonUserDetails.getPassword(),
					commonUserDetails.getUserInfoId());
			if (!isCurrentPasswordExist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("password.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isnewconfirmpasswordsame = userRegistrationService.isNewConfirmPasswordSame(
					commonUserDetails.getNewPassword(), commonUserDetails.getConfirmPassword());
			if (!isnewconfirmpasswordsame) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("new.confirm.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean savePassword = userRegistrationService.savePassword(commonUserDetails.getConfirmPassword(),
					commonUserDetails.getUserInfoId());
			if (!savePassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("save.password.error"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("save.password.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/forgotPassword/{email}", produces = "application/json")
	@ApiOperation(value = "Check email", notes = "Email status")
	public ResponseEntity<String> checkEmail(@ApiParam(value = "Required user mailId") @PathVariable String email) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean isEmailExist = userRegistrationService.isEmailExist(email);
			if (!isEmailExist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("mailid.notexist.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				String randomPassword = registrationUtils.randongStringGenration();

				// save randompassword below
				String encryptedCode = registrationUtils.bcryptPassword(randomPassword);
				userRegistrationService.saveRandomPassword(encryptedCode, email);

				// logic to send otp email
				boolean emailSent = emailNotificationUtils.sendForgotPasswordEmail(email, "Your Random Password",
						randomPassword, "Narayanan");
				if (emailSent) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("forgotPwd.otp.success"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("sendotp.faliure"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}

			}

		} catch (Exception e) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("internal.server.error"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	/*
	 * @PostMapping(value = "/saveimage", produces = "application/json") public
	 * ResponseEntity<String> saveimg(@RequestParam("imageFile") MultipartFile
	 * imageFile){ StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
	 * //System.out.println("commonUserDetails"+commonUserDetails);
	 * System.out.println("imageFile"+imageFile);
	 * 
	 * CommonUserDetails commonUserInfo = null; try {
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); //commonUserInfo =
	 * mapper.readValue(commonUserDetails, CommonUserDetails.class);
	 * //userRegistrationService.saveImg(imageFile);
	 * statusResponseDTO.setStatus(env.getProperty("success"));
	 * 
	 * return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.OK);
	 * 
	 * } catch (Exception e) { e.printStackTrace(); log.info("Exception :" +
	 * e.getMessage()); statusResponseDTO.setStatus(env.getProperty("failure"));
	 * statusResponseDTO.setMessage(env.getProperty("validate.register.faild"));
	 * return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.PARTIAL_CONTENT); } //UserInfo userInfo =
	 * userRegistrationService.saveImg(commonUserInfo, imageFile);
	 * 
	 * 
	 * }
	 */

}
