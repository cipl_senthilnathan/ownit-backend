package com.ownit.common.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.SocialSitesDTO;
import com.ownit.common.dto.StatusResponseDTO;
import com.ownit.common.dto.UpdateLoginDetailsDTO;
import com.ownit.common.service.UserServiceList;
import com.ownit.common.service.UserRegistrationService;
import com.ownit.common.utils.EmailNotificationUtils;
import com.ownit.common.utils.RegistrationUtils;
import com.ownit.dataaccess.model.LoginInfo;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.repository.LogInfoRepository;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@RequestMapping("Api/UserList")
@CrossOrigin
public class UserControllerList {

	private static final Logger log = LoggerFactory.getLogger(UserControllerList.class);

	@Autowired
	Environment env;

	@Autowired
	UserServiceList listUserService;

	@Autowired
	EmailNotificationUtils emailNotificationUtils;

	@Autowired
	RegistrationUtils registrationUtils;

	@Autowired
	LogInfoRepository loginInfoRepository;

	@GetMapping(value = "/userlist", produces = "application/json")
	public ResponseEntity<String> userList() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {

			List<UserInfo> userList = listUserService.UserDataList();
			if (userList != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("userlist.data.success"));
				statusResponseDTO.setListUserData(userList);

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("userlist.data.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Problem in registration  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

}
