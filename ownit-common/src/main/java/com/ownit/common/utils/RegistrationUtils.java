package com.ownit.common.utils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.ProductInfoDTO;
import com.ownit.common.dto.SocialSitesDTO;
import com.ownit.common.dto.UpdateLoginDetailsDTO;

@Service

public class RegistrationUtils {

	private static final Logger log = LoggerFactory.getLogger(RegistrationUtils.class);
	static final String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

	public static boolean inputValidation(CommonUserDetails commonUserInfo) {

		if ((commonUserInfo.getFirstName() != null && StringUtils.isNotBlank(commonUserInfo.getFirstName()))
				&& (commonUserInfo.getLastName() != null && StringUtils.isNotBlank(commonUserInfo.getLastName()))

				&& (commonUserInfo.getEmail() != null && StringUtils.isNotBlank(commonUserInfo.getEmail()))

				&& (commonUserInfo.getMobileNumber() != null
						&& StringUtils.isNotBlank(commonUserInfo.getMobileNumber()))) {

			return true;

		}

		return false;
	}

	public static boolean productInfoInputValidation(ProductInfoDTO productInfoDTO) {

		if ((productInfoDTO.getCategories() != null && StringUtils.isNotBlank(productInfoDTO.getCategories()))
				&& (productInfoDTO.getExtraShippingRate() != null)
				&& (productInfoDTO.getImage() != null && StringUtils.isNotBlank(productInfoDTO.getImage()))
				&& (productInfoDTO.getInventoryQuantity() != null) && (productInfoDTO.getInventorySku() != null)
				&& (productInfoDTO.getPricingComparedPrice() != null)
				&& (productInfoDTO.getPricingExcludedPrice() != null)
				&& (productInfoDTO.getPricingIncludedPrice() != null) && (productInfoDTO.getPricingTaxRate() != null)
				&& (productInfoDTO.getProductDescription() != null
						&& StringUtils.isNotBlank(productInfoDTO.getProductDescription()))
				&& (productInfoDTO.getProductName() != null && StringUtils.isNotBlank(productInfoDTO.getProductName()))
				&& (productInfoDTO.getShippingHeight() != null) && (productInfoDTO.getShippingTaxRate() != null)
				&& (productInfoDTO.getShippingWeight() != null) && (productInfoDTO.getShippingWidth() != null)
				&& (productInfoDTO.getTags() != null && StringUtils.isNotBlank(productInfoDTO.getTags()))) {

			return true;

		}

		return false;
	}

	// To validate Email
	public static boolean validateEmail(String email_id) {
		email_id = email_id.replaceFirst("^ *", "");
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email_id);
		log.info(email_id + " : " + matcher.matches());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean socialInputValidation(SocialSitesDTO socialSitesDTO) {

		if ((socialSitesDTO.getFirstName() != null && StringUtils.isNotBlank(socialSitesDTO.getFirstName()))
				&& (socialSitesDTO.getLastName() != null && StringUtils.isNotBlank(socialSitesDTO.getLastName()))
				&& (socialSitesDTO.getUserId() != null && StringUtils.isNotBlank(socialSitesDTO.getUserId()))
				&& (socialSitesDTO.getUniqueId() != null && StringUtils.isNotBlank(socialSitesDTO.getUniqueId()))
				&& (socialSitesDTO.getProvider() != null && StringUtils.isNotBlank(socialSitesDTO.getProvider()))) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean resetPasswordInputValidation(CommonUserDetails commonUserDetails) {

		if ((commonUserDetails.getPassword() != null && StringUtils.isNotBlank(commonUserDetails.getPassword()))
				&& (commonUserDetails.getUserInfoId() != null)
				&& (commonUserDetails.getNewPassword() != null
						&& StringUtils.isNotBlank(commonUserDetails.getNewPassword()))
				&& (commonUserDetails.getConfirmPassword() != null
						&& StringUtils.isNotBlank(commonUserDetails.getConfirmPassword()))) {
			return true;
		} else {
			return false;
		}
	}

	public String randongStringGenration() {

		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 8) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		log.info("randomString :" + saltStr);
		return saltStr;

	}

	public String bcryptPassword(String randomStr) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(randomStr);
		return hashedPassword;

	}

	public static boolean validateLoginInfo(UpdateLoginDetailsDTO updateLoginDetailsDTO) {

		if ((updateLoginDetailsDTO.getUserName() != null && StringUtils.isNotBlank(updateLoginDetailsDTO.getUserName()))
				&& (updateLoginDetailsDTO.getTempPassword() != null
						&& StringUtils.isNotBlank(updateLoginDetailsDTO.getTempPassword()))
				&& (updateLoginDetailsDTO.getPassword() != null
						&& StringUtils.isNotBlank(updateLoginDetailsDTO.getPassword()))
				&& (updateLoginDetailsDTO.getConfirmPassword() != null
						&& StringUtils.isNotBlank(updateLoginDetailsDTO.getConfirmPassword()))) {

			return true;
		}

		return false;

	}

	public static boolean checkPassword(String temppwd, String randompwd) {
		log.info("temppwd:" + temppwd);
		log.info("randompwd: " + randompwd);
		boolean password_verified = false;

		if (null == randompwd || !randompwd.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = BCrypt.checkpw(temppwd, randompwd);

		return (password_verified);
	}

}
