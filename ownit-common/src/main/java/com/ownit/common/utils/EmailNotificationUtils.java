package com.ownit.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@SuppressWarnings("serial")
@Service
public class EmailNotificationUtils extends HttpServlet {
	private static final Logger LOG = LoggerFactory.getLogger(EmailNotificationUtils.class);

	@Autowired
	private Environment envi;

	@Value("${email.host}")
	private String host;

	@Value("${email.port}")
	private Integer port;

	@Value("${email.username}")
	private String username;

	@Value("${email.password}")
	private String password;

	@Value("${spring.mail.transport.protocol}")
	private String transportProtocol;

	/*
	 * @Value("${env}") private String env;
	 */

	/*
	 * @Value("${email.path}") private String emailPath;
	 */

	// Supply your SMTP credentials below. Note that your SMTP credentials are
	// different from your AWS credentials.
	@Value("${smtp.username}")
	private String SMTP_USERNAME;

	@Value("${smtp.password}")
	private String SMTP_PASSWORD;

	public boolean sendVerifyEmailOther(String toEamilId, String subject, String userName) {

		try {
			LOG.info("In  sendEmailDEV  STARTS:Others ");

			LOG.info("toEamilId: " + toEamilId + " subject: " + subject + " userName: " + userName);

			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEamilId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			MimeMultipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			InternetAddress[] toAddresses = { new InternetAddress(toEamilId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			Map<String, String> input = new HashMap<String, String>();
			input.put("tushar.goyal@mansudsales.com", toEamilId);
			// input.put("ImagePath", envi.getProperty("image.mailer.path"));
			input.put("newuser", userName);
			// input.put("contactusLink", envi.getProperty("contactusLink"));
			input.put("clickcantactus", ("contact@epaperroad.com"));
			// input.put("activateLink", envi.getProperty("verify.mailId")+toEamilId);
			input.put("actMessage", ("Click here to activate your account"));
			File file = new ClassPathResource("otherrequesterregistration.html").getFile();

			String htmlText = readEmailFromHtml(file.getAbsolutePath(), input);
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			// sends the e-mail

			LOG.info("Attempting to send an email");

			Transport.send(msg);

			LOG.info("Email sent successfully !");
			// LOG.info("In sendEmailDEV END: " + env);
			return true;
		} catch (MailException e) {
			// LOG.info("Problem in sending mail sendEmailDEV END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			// LOG.info("Problem in sending mail sendEmailDEV END: " + env);
			e.printStackTrace();
			return false;
		}
	}

	public boolean sendForgotPasswordEmail(String toEamilId, String subject, String RandomPassword, String name) {
		try {

			// LOG.info("In sendEmailDEV STARTS: " + env);

			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEamilId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			MimeMultipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			InternetAddress[] toAddresses = { new InternetAddress(toEamilId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			Map<String, String> input = new HashMap<String, String>();
			// input.put("ImagePath", envi.getProperty("image.mailer.path"));
			// input.put("contactusLink", envi.getProperty("contactusLink"));
			input.put("clickcantactus", ("contact@epaperroad.com"));
			input.put("newuser", name);
			input.put("Username", toEamilId);
			input.put("RandomPassword", RandomPassword);
			File file = new ClassPathResource("resetpassword.html").getFile();
			String htmlText = readEmailFromHtml(file.getAbsolutePath(), input);
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			// sends the e-mail

			LOG.info("Attempting to send an email");

			Transport.send(msg);

			LOG.info("Email sent successfully !");
			// LOG.info("In sendEmailDEV END: " + env);
			return true;
		} catch (MailException e) {
			// LOG.info("Problem in sending mail sendEmailDEV END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			// LOG.info("Problem in sending mail sendEmailDEV END: " + env);
			e.printStackTrace();
			return false;
		}
	}

	// Method to replace the values for keys
	protected String readEmailFromHtml(String filePath, Map<String, String> input) {
		String msg = readContentFromFile(filePath);
		try {
			Set<Entry<String, String>> entries = input.entrySet();
			for (Map.Entry<String, String> entry : entries) {
				msg = msg.replace(entry.getKey().trim(), entry.getValue().trim());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}

	private String readContentFromFile(String fileName) {
		StringBuffer contents = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
					// System.out.println("hello");
				}
			} finally {
				reader.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return contents.toString();
	}

}
