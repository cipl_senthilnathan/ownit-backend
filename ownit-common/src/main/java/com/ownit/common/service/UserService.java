package com.ownit.common.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

//import com.epaper.dataaccess.model.LoginInfo;
import com.ownit.dataaccess.model.UserInfo;

public interface UserService {

	/*
	 * LoginInfo save(LoginInfo user);
	 * 
	 * List<LoginInfo> findAll();
	 */

	void delete(long id);

	UserDetails loadUserByUsername(String username);

	/*
	 * LoginInfo updateLoginPassword(UpdateLoginDetailsDTO updatePass);
	 * 
	 * void saveRandomPwd(String hashedPassword, String username);
	 * 
	 * LoginInfo getLoginDetails(String username);
	 * 
	 * public boolean emailExistOrNot(String email);
	 * 
	 * public boolean mobileExistOrNot(String mobile);
	 * 
	 * List<StateRegisterDTO> searchForLocationregister(String location);
	 * 
	 * boolean contactMailSent(ContactDTO contactDto);
	 * 
	 * public boolean activateUser(CommonUserDTO commonUserDTO);
	 * 
	 * UserInfo getUserDetails(String username);
	 * 
	 * boolean resendVerificationLink(UserInfo userInfo);
	 */

}
