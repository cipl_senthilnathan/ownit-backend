package com.ownit.common.service.impl;

import java.util.ArrayList;


import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/*import com.epaper.common.DTO.CommonUserDTO;
import com.epaper.common.DTO.ContactDTO;
import com.epaper.common.DTO.StateRegisterDTO;
import com.epaper.common.DTO.UpdateLoginDetailsDTO;
import com.epaper.common.controller.RegistrationController;
import com.epaper.common.dao.LoginDaoService;
import com.epaper.common.utils.EmailNotificationUtils;*/


import com.ownit.common.service.UserService;

import com.ownit.dataaccess.model.LoginInfo;
//import com.epaper.dataaccess.model.State;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.repository.LogInfoRepository;
//import com.epaper.dataaccess.repository.StateRepository;
import com.ownit.dataaccess.repository.UserInfoRepository;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	//private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);
	
	@Autowired
	private LogInfoRepository logInfoRepository;
	
	
	
	@Autowired
	private Environment env;
	
	
/*	@Autowired
	private LoginDaoService loginDaoService;
	@Autowired
	private StateRepository stateRepository;
	
	 @Autowired
	private EmailNotificationUtils emailNotificationUtils;*/
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	
	

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		LoginInfo user = logInfoRepository.findByEmailOrMobileNo(userId,userId);
		if(user == null){
			
			throw new UsernameNotFoundException(env.getProperty("login.failed"));
			
		}
		
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
		
	}

	public List<LoginInfo> findAll() {
		
		List<LoginInfo> list = new ArrayList<>();
		logInfoRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
		
	}

	@Override
	public void delete(long id) {
		//userDao.delete(id);
	}

	
	
}
