package com.ownit.common.service;

import java.util.List;
import com.ownit.common.dto.CommonUserDetails;
import com.ownit.dataaccess.model.UserInfo;

public interface UserServiceList {
	List<UserInfo> UserDataList();

}
