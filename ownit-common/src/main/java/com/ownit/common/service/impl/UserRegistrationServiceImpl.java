package com.ownit.common.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.springframework.core.env.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mysql.cj.util.Base64Decoder;
import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.SocialSitesDTO;
import com.ownit.common.service.UserRegistrationService;
import com.ownit.common.utils.EmailNotificationUtils;
import com.ownit.dataaccess.model.LoginInfo;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.model.UserTypeInfo;
import com.ownit.dataaccess.repository.LogInfoRepository;
import com.ownit.dataaccess.repository.UserInfoRepository;
import com.ownit.dataaccess.repository.UserTypeInfoRepository;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

	private static final Logger log = LoggerFactory.getLogger(UserRegistrationServiceImpl.class);

	@Autowired
	UserInfoRepository userInfoRepository;

	@Autowired
	LogInfoRepository logInfoRepository;

	@Autowired
	UserTypeInfoRepository userTypeInfoRepository;

	@Autowired
	EmailNotificationUtils emailNotificationUtils;
	@Autowired
	Environment env;

	@Override
	public String saveUser(CommonUserDetails commonUserInfo) {

		try {

			UserInfo userInfo = new UserInfo();

			userInfo.setFirstName(commonUserInfo.getFirstName());
			userInfo.setLastName(commonUserInfo.getLastName());
			userInfo.setEmail(commonUserInfo.getEmail());
			userInfo.setMobile(commonUserInfo.getMobileNumber());
			userInfo.setRegistrationStatus("Pending");

			log.info("user type info =======>" + commonUserInfo.getUserType());

			UserTypeInfo UserTypeInfo = userTypeInfoRepository.findByUserType(commonUserInfo.getUserType());

			userInfo.setUserTypeInfo(UserTypeInfo);
			userInfo = userInfoRepository.save(userInfo);

			LoginInfo loginInfo = new LoginInfo();
			loginInfo.setEmail(commonUserInfo.getEmail());
			loginInfo.setMobileNo(commonUserInfo.getMobileNumber());
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

			String hashedPassword = passwordEncoder.encode(commonUserInfo.getPassword());
			log.info("hash password in save :" + hashedPassword);
			loginInfo.setPassword(hashedPassword);
			loginInfo.setUserInfo(userInfo);
			logInfoRepository.save(loginInfo);

			log.info("getemail id " + commonUserInfo.getEmail() + "first name " + commonUserInfo.getFirstName()
					+ "last name " + commonUserInfo.getLastName());

			// send mail content here
			emailNotificationUtils.sendVerifyEmailOther(commonUserInfo.getEmail(), "Verify Email",
					commonUserInfo.getLastName() + " " + commonUserInfo.getFirstName());

			return "success";

		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}

	@Override
	public List<UserInfo> checkEmailExist(String email) {
		List<UserInfo> userDetails = (List<UserInfo>) userInfoRepository.findByEmail(email);

		return userDetails;
	}

	@Override
	public boolean changePassword(String email, String password) {
		LoginInfo user = logInfoRepository.findByEmail(email);
		if (user != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(password);
			user.setPassword(hashedPassword);
			logInfoRepository.save(user);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isEmailExist(String email) {
		UserInfo userInfo = userInfoRepository.findUserInfoByEmailIgnoreCase(email);

		if (userInfo == null) {
			return false;
		} else {
			return true;
		}

	}

	@Override
	public boolean checkRegistrationStatus(String email) {
		UserInfo userInfo = userInfoRepository.findUserInfoByEmailIgnoreCase(email);

		if (userInfo != null && !userInfo.getRegistrationStatus().equalsIgnoreCase("Completed")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String verifyUser(String email) {

		try {

			log.info("Mail Id =======>" + email);

			UserInfo userInfo = userInfoRepository.findUserInfoByEmailIgnoreCase(email);

			userInfo.setRegistrationStatus("Completed");
			userInfo = userInfoRepository.save(userInfo);

			return "success";

		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}

	}

	@Override
	public boolean socialSiteSave(SocialSitesDTO socialSiteInfo) {
		try {

			UserInfo userInfo = new UserInfo();
			userInfo.setFirstName(socialSiteInfo.getFirstName());
			userInfo.setLastName(socialSiteInfo.getLastName());
			userInfo.setEmail(socialSiteInfo.getUserId());

			if (socialSiteInfo.getProvider().equalsIgnoreCase("gmail")) {
				// userInfo.setGmailId(socialSiteInfo.getUniqueId());
				userInfo.setProvider(socialSiteInfo.getProvider());
				userInfo.setUniqueId(socialSiteInfo.getUniqueId());
			} else if (socialSiteInfo.getProvider().equalsIgnoreCase("facebook")) {
				// userInfo.setFbId(socialSiteInfo.getUniqueId());
				userInfo.setProvider(socialSiteInfo.getProvider());
				userInfo.setUniqueId(socialSiteInfo.getUniqueId());
			}

			log.info("socialSiteInfo.getUserType()====>" + socialSiteInfo.getUserType());
			UserTypeInfo UserTypeInfo = userTypeInfoRepository.findByUserType("user");

			userInfo.setUserTypeInfo(UserTypeInfo);
			// userId encrypted
			// userId value
			// provider store

			userInfo = userInfoRepository.save(userInfo);

			LoginInfo loginInfo = new LoginInfo();

			loginInfo.setEmail(socialSiteInfo.getUserId());
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(socialSiteInfo.getUniqueId());
			loginInfo.setPassword(hashedPassword);
			loginInfo.setUserInfo(userInfo);

			logInfoRepository.save(loginInfo);

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean isEmailExistInUserLoginInfo(String email) {
		UserInfo userInfo = userInfoRepository.findUserInfoByEmailIgnoreCase(email);
		LoginInfo loginInfo = logInfoRepository.findByEmail(email);

		if (userInfo == null && loginInfo == null) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean userProfile(CommonUserDetails commonUserDetails) {
		try {
			UserInfo userInfo = userInfoRepository.findById(commonUserDetails.getUserId());
			userInfo.setFirstName(commonUserDetails.getFirstName());
			userInfo.setLastName(commonUserDetails.getLastName());
			userInfo.setEmail(commonUserDetails.getEmail());
			userInfo.setMobile(commonUserDetails.getMobileNumber());
			userInfo.setAddress(commonUserDetails.getAddress());
			userInfoRepository.save(userInfo);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	@Override
	public boolean isPasswordExist(String password, Long userInfoId) {
		UserInfo userInfo = userInfoRepository.findById(userInfoId);
		LoginInfo loginInfo = logInfoRepository.findLoginInfoByUserInfo(userInfo);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		encoder.matches(password, loginInfo.getPassword());
		if (encoder.matches(password, loginInfo.getPassword()) == true) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean isLoginInfoExist(Long userInfoId) {

		UserInfo userInfo = userInfoRepository.findById(userInfoId);
		// LoginInfo loginInfo = logInfoRepository.findLogInfoById(userInfoId);
		if (userInfo != null) {
			LoginInfo loginInfo = logInfoRepository.findLoginInfoByUserInfo(userInfo);
			return true;

		} else {
			return false;
		}

	}

	@Override
	public boolean isNewConfirmPasswordSame(String newPassword, String confirmPassword) {
		if (newPassword.equals(confirmPassword)) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean savePassword(String confirmPassword, Long userInfoId) {
		UserInfo userInfo = userInfoRepository.findById(userInfoId);
		LoginInfo loginInfo = logInfoRepository.findLoginInfoByUserInfo(userInfo);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if (userInfo != null && loginInfo != null) {

			if (userInfo.getProvider() == null) {
				String hashPassword = passwordEncoder.encode(confirmPassword);
				loginInfo.setPassword(hashPassword);

			} else if (userInfo.getProvider().equalsIgnoreCase("gmail") == true) {
				// userInfo.setGmailId(confirmPassword);
				userInfo.setUniqueId(confirmPassword);
				userInfoRepository.save(userInfo);
				String hashPassword = passwordEncoder.encode(confirmPassword);
				loginInfo.setPassword(hashPassword);

			} else if (userInfo.getProvider().equalsIgnoreCase("facebook") == true) {
				// userInfo.setFbId(confirmPassword);
				userInfo.setUniqueId(confirmPassword);
				userInfoRepository.save(userInfo);
				String hashPassword = passwordEncoder.encode(confirmPassword);
				loginInfo.setPassword(hashPassword);
			}
			logInfoRepository.save(loginInfo);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String saveRandomPassword(String encryptedCode, String userId) {
		try {

			LoginInfo logInfo = logInfoRepository.findByEmail(userId);
			logInfo.setRandomPassword(encryptedCode);
			logInfoRepository.save(logInfo);

			return "success";

		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}

	}

	public boolean savePicture(MultipartFile imagefile) {
		/*
		 * FileInputStream reader = null; FileOutputStream writer = null; String path =
		 * null; System.out.println("impl-----------44444444444444444");
		 */
		String basePath = env.getProperty("image.upload.path");
		try {
			String fileName = imagefile.getOriginalFilename();
			InputStream is = imagefile.getInputStream();
			String name = basePath + fileName;
			File dire = new File(name);

			if (!dire.exists()) {
				dire.createNewFile();
			} else {

			}
			imagefile.transferTo(dire);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;

		}
	}

	@Override
	public boolean saveProfileImg(CommonUserDetails commonUserInfo, MultipartFile imageFile) throws IOException {
		
			String basePath = env.getProperty("image.upload.path");
			String fileName = imageFile.getOriginalFilename();
			String name = basePath + fileName;
			File dire = new File(name);
			if (!dire.exists()) {
				dire.createNewFile();
			}

			imageFile.transferTo(dire);
			
			UserInfo userInfo = userInfoRepository.findById(commonUserInfo.getUserTypeId());
			userInfo.setFirstName(commonUserInfo.getFirstName());
			userInfo.setLastName(commonUserInfo.getLastName());
			userInfo.setEmail(commonUserInfo.getEmail());
			userInfo.setMobile(commonUserInfo.getMobileNumber());
			userInfo.setProfileImage(name);
			userInfoRepository.save(userInfo);
            return true;

	}

}
