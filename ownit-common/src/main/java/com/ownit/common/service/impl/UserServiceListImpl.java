package com.ownit.common.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.service.UserServiceList;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.repository.ListUserRepository;

@Service
public class UserServiceListImpl implements UserServiceList {
	private static final Logger log = LoggerFactory.getLogger(UserServiceListImpl.class);

	@Autowired
	private ListUserRepository listUserRepository;

	@Override
	public List<UserInfo> UserDataList() {
		List<CommonUserDetails> commonUserDetailsList = new ArrayList<CommonUserDetails>();
		try {
			List<UserInfo> userInfoList = listUserRepository.findAll();
			if (userInfoList != null) {

				for (UserInfo userInfo : userInfoList) {

					/* CommonUserDetails CommonUserDetails1 = new CommonUserDetails();
					 * CommonUserDetails1.setFirstName(userInfo.getFirstName());
					 * CommonUserDetails1.setLastName(userInfo.getLastName());
					 * CommonUserDetails1.setMobileNumber(userInfo.getMobile());
					 * CommonUserDetails1.setEmail(userInfo.getEmail());
					 * CommonUserDetails1.setProvider(userInfo.getProvider());
					 * commonUserDetailsList.add(CommonUserDetails1);*/
					return userInfoList;
				}
          } 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}