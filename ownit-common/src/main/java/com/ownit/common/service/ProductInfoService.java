package com.ownit.common.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ownit.common.dto.ProductInfoDTO;
import com.ownit.dataaccess.model.ProductInfo;
import com.ownit.dataaccess.model.UserInfo;

@Service
public interface ProductInfoService {

	boolean saveProductInfo(ProductInfoDTO productInfoDTO, MultipartFile imageFile) throws IOException;
	List<ProductInfo> productList();
}
