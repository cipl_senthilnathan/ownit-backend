package com.ownit.common.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Multipart;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.ProductInfoDTO;
import com.ownit.common.service.ProductInfoService;
import com.ownit.dataaccess.model.ProductInfo;
import com.ownit.dataaccess.model.UserInfo;
import com.ownit.dataaccess.repository.ProductInfoRepository;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {

	private static final Logger log = LoggerFactory.getLogger(ProductInfoServiceImpl.class);

	@Autowired
	ProductInfoRepository productInfoRepository;

	@Autowired
	Environment env;

	@Override
	public boolean saveProductInfo(ProductInfoDTO productInfoDTO, MultipartFile imageFile) throws IOException {

		String basePath = env.getProperty("image.upload.path");
		String fileName = imageFile.getOriginalFilename();
		String name = basePath + fileName;
		String imageString = "";
		File dire = new File(name);

		if (!dire.exists()) {
			dire.createNewFile();
		}

		imageFile.transferTo(dire);

		ProductInfo productInfo = new ProductInfo();

		productInfo.setCategories(productInfoDTO.getCategories());
		productInfo.setExtraShippingRate(productInfoDTO.getExtraShippingRate());
		//productInfo.setImage(name);
		productInfo.setInventoryQuantity(productInfoDTO.getInventoryQuantity());
		productInfo.setInventorySku(productInfoDTO.getInventorySku());
		productInfo.setPricingComparedPrice(productInfoDTO.getPricingComparedPrice());
		productInfo.setPricingExcludedPrice(productInfoDTO.getPricingExcludedPrice());
		productInfo.setPricingIncludedPrice(productInfoDTO.getPricingIncludedPrice());
		productInfo.setPricingTaxRate(productInfoDTO.getPricingTaxRate());
		productInfo.setProductDescription(productInfoDTO.getProductDescription());
		productInfo.setProductName(productInfoDTO.getProductName());
		productInfo.setShippingHeight(productInfoDTO.getShippingHeight());
		productInfo.setShippingTaxRate(productInfoDTO.getShippingTaxRate());
		productInfo.setShippingWeight(productInfoDTO.getShippingWeight());
		productInfo.setShippingWidth(productInfoDTO.getShippingWidth());
		productInfo.setTags(productInfoDTO.getTags());
//		try {
//
//			imageString = Base64.encodeBase64String(
//					FileUtils.readFileToByteArray(new File(env.getProperty("image.upload.path") + productInfo.getImage())));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		productInfo.setImage(name);
        productInfoRepository.save(productInfo);
		return true;
	}

	public List<ProductInfo> productList() {
		
			List<ProductInfoDTO> productInfoDTO = new ArrayList<ProductInfoDTO>();
			try {
				List<ProductInfo> productInfoList = productInfoRepository.findAll();
				if (productInfoList != null) {

					for (ProductInfo productInfo : productInfoList) {

						/* CommonUserDetails CommonUserDetails1 = new CommonUserDetails();
						 * CommonUserDetails1.setFirstName(userInfo.getFirstName());
						 * CommonUserDetails1.setLastName(userInfo.getLastName());
						 * CommonUserDetails1.setMobileNumber(userInfo.getMobile());
						 * CommonUserDetails1.setEmail(userInfo.getEmail());
						 * CommonUserDetails1.setProvider(userInfo.getProvider());
						 * commonUserDetailsList.add(CommonUserDetails1);*/
						return productInfoList;
					}
	          } 
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

}
