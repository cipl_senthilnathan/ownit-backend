package com.ownit.common.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ownit.common.dto.CommonUserDetails;
import com.ownit.common.dto.SocialSitesDTO;
import com.ownit.dataaccess.model.UserInfo;

//@Service
public interface UserRegistrationService {

	String saveUser(CommonUserDetails commonUserInfo);

	List<UserInfo> checkEmailExist(String email);

	// boolean isAccountExistCheckByEmailId(String email);
	boolean changePassword(String email, String password);

	String verifyUser(String email);

	boolean isEmailExist(String email);

	boolean isEmailExistInUserLoginInfo(String email);

	boolean checkRegistrationStatus(String email);

	// social site info save
	boolean socialSiteSave(SocialSitesDTO socialSiteInfo);

	boolean userProfile(CommonUserDetails commonUserInfo);

	boolean isNewConfirmPasswordSame(String newPassword, String confirmPassword);

	boolean isPasswordExist(String password, Long loginId);

	boolean savePassword(String confirmPassword, Long loginId);

	boolean isLoginInfoExist(Long loginId);

	String saveRandomPassword(String encryptedCode, String email);

	boolean savePicture(MultipartFile imageFile);

	boolean saveProfileImg(CommonUserDetails commonUserInfo, MultipartFile imageFile) throws IOException;

}
