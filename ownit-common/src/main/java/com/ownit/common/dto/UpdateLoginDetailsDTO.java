package com.ownit.common.dto;

import lombok.Data;

@Data
public class UpdateLoginDetailsDTO {

	private String userName;

	private String tempPassword;

	private String password;

	private String confirmPassword;
}
