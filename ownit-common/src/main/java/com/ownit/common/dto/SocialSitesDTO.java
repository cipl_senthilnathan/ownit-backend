package com.ownit.common.dto;

import lombok.Data;

@Data
public class SocialSitesDTO {

	private String firstName;

	private String lastName;

	private String uniqueId;

	private String userId;

	private String provider;

	private String userType;

}
