package com.ownit.common.dto;

import lombok.Data;

@Data
public class CommonUserDetails {

	private String firstName;
	private Long userId;

	private String lastName;

	private String email;

	private String address;
	private String mobileNumber;

	private String password;

	private String userType;

	private String newPassword;
    private Long userTypeId;
	private String confirmPassword;
	private String Provider;
	private Long userInfoId;
	private String profileImage;

}
