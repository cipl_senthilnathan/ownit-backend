package com.ownit.common.dto;

import lombok.Data;

@Data
public class ProductInfoDTO {

	private Long id;
	private String productName;
	private String productDescription;
	private String categories;
	private String tags;
	private String image;
	private Long pricingExcludedPrice;
	private Long pricingIncludedPrice;
	private Long pricingTaxRate;
	private Long pricingComparedPrice;
	private Long inventorySku;
	private Long inventoryQuantity;
	private Long shippingWidth;
	private Long shippingHeight;
	private Long shippingWeight;
	private Long shippingTaxRate;
	private Long extraShippingRate;
}
