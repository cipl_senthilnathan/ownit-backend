package com.ownit.common.dto;

import java.util.List;

import com.ownit.dataaccess.model.ProductInfo;
import com.ownit.dataaccess.model.UserInfo;

import lombok.Data;

@Data
public class StatusResponseDTO {

	private String status;
	private String message;
	List<UserInfo> listUserData;
	List<ProductInfo> productListData;

}
