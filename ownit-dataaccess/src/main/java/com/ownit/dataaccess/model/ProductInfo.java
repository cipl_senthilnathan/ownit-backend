package com.ownit.dataaccess.model;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "product_info")
@Data
@NamedQuery(name= "ProductInfo.findAll" , query = "SELECT p FROM ProductInfo p ")
public class ProductInfo {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
	private Long id;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column(name = "product_description")
	private String productDescription;
	
	@Column(name = "categories")
	private String categories;
	
	@Column(name = "tags")
	private String tags;
	
	@Column(name = "image")
	private String image;
	
	@Column(name = "pricing_excluded_price")
	private Long pricingExcludedPrice;
	
	@Column(name = "pricing_included_price")
	private Long pricingIncludedPrice	;
	
	@Column(name = "pricing_tax_rate")
	private Long pricingTaxRate;
	
	@Column(name = "pricing_compared_price")
	private Long pricingComparedPrice	;
	
	@Column(name = "inventory_sku")
	private Long inventorySku	;	
	
	@Column(name = "inventory_quantity")
	private Long inventoryQuantity	;
	
	@Column(name = "shipping_width")
	private Long shippingWidth	;	
	
	@Column(name = "shipping_height")
	private Long shippingHeight	;	
	
	@Column(name = "shipping_weight")
	private Long shippingWeight	;		
	
	@Column(name = "shipping_tax_rate")
	private Long shippingTaxRate	;		
	
	@Column(name = "extra_shipping_rate")
	private Long extraShippingRate	;			
	
	
}

