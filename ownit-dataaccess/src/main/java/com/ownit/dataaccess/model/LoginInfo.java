package com.ownit.dataaccess.model;


import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;


/**
 * The persistent class for the login_info database table.
 * 
 */
@Entity
@Table(name="login_info")
@Data
@NamedQuery(name="LoginInfo.findAll", query="SELECT l FROM LoginInfo l")
public class LoginInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
	private Long id;

	@Column
	private String email;

	@Column(name = "mobile")
	private String mobileNo;

	@Column
	private String password;
	
	@Column(name = "random_pwd")
	private String randomPassword;

	/*//bi-directional many-to-one association to EmployeeInfo
	@ManyToOne
	@JoinColumn(name="employee_info_id")
	private EmployeeInfo employeeInfo;

	//bi-directional many-to-one association to FacilityInfo
	@ManyToOne
	@JoinColumn(name="facility_info_id")
	private FacilityInfo facilityInfo;*/

	//bi-directional many-to-one association to UserInfo
	@ManyToOne
	@JoinColumn(name="user_info_id")
	private UserInfo userInfo;

	
	
}