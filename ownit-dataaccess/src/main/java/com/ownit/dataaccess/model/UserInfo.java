package com.ownit.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the user_info database table.
 * 
 */
@Entity
@Table(name = "user_info")
@Data
@NamedQuery(name = "UserInfo.findAll", query = "SELECT u FROM UserInfo u")
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;

	private String email;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "address")
	private String address;

	// private String address2;

	// private String city;

	// private String state;

	// private String zip;

	private String mobile;

	// private String fileName;

	private String registrationStatus;

	private String uniqueId;

	private String provider;

	/*
	 * @Column(name="comapany_name") private String companyName;
	 */

	@Lob
	@Column(name = "signed_form", columnDefinition = "BLOB")
	private byte[] signedForm;

	// bi-directional many-to-one association to UserInfo
	@ManyToOne
	@JoinColumn(name = "user_type_id")
	private UserTypeInfo userTypeInfo;
	
	@Column(name = "profile_image")
	private String profileImage;

}