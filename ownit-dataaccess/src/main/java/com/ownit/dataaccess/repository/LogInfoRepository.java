package com.ownit.dataaccess.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

//import com.ownit.dataaccess.model.EmployeeInfo;
import com.ownit.dataaccess.model.LoginInfo;
import com.ownit.dataaccess.model.UserInfo;

@Repository
public interface LogInfoRepository extends CrudRepository<LoginInfo, Long> {

	public LoginInfo findByEmailOrMobileNo(String email, String mobileNo);

	public LoginInfo findLogInfoById(Long id);

	public LoginInfo findLoginInfoByUserInfo(UserInfo userInfo);

	// public LoginInfo findLoginInfoByEmployeeInfo(EmployeeInfo employeeInfo);

	public LoginInfo findByEmail(String emailId);

	public LoginInfo findByMobileNo(String mobileNo);

	// public LoginInfo findLogInfoByEmployeeInfo(EmployeeInfo employeeInfo);
}
