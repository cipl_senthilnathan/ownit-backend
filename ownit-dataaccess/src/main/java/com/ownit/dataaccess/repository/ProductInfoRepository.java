package com.ownit.dataaccess.repository;

import org.springframework.stereotype.Repository;

import com.ownit.dataaccess.model.ProductInfo;
import com.ownit.dataaccess.model.UserInfo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface ProductInfoRepository extends CrudRepository<ProductInfo, Long> {
	List<ProductInfo> findAll();


}
