package com.ownit.dataaccess.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ownit.dataaccess.model.UserInfo;

@Repository
public interface ListUserRepository extends CrudRepository<UserInfo, Long> {

	List<UserInfo> findAll();
}