package com.ownit.dataaccess.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.ownit.dataaccess.model.UserTypeInfo;

@Repository
public interface UserTypeInfoRepository extends CrudRepository<UserTypeInfo, Integer> {

	public UserTypeInfo findUserTypeInfoById(Long id);

	public UserTypeInfo findByUserType(String userType);

}
