package com.ownit.dataaccess.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ownit.dataaccess.model.UserInfo;

@Repository
public interface UserInfoRepository extends CrudRepository<UserInfo, String> {

	List<UserInfo> findByEmailOrMobile(String email, String mobile);

	List<UserInfo> findByEmail(String email);

	UserInfo findById(long userId);

	public UserInfo findUserInfoById(Long id);

	UserInfo findUserInfoById(long id);

	public UserInfo findUserInfoByEmailIgnoreCase(String email);

	// below is for resendverification link
	UserInfo findByMobileOrEmail(String email, String mobile);

	// forgot pwd
	UserInfo findByEmailEqualsIgnoreCase(String email);

}
