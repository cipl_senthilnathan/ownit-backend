DROP TABLE IF EXISTS `product_info`;

CREATE TABLE `product_info` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `product_name` VARCHAR(45) NULL,
  `product_description` VARCHAR(45) NULL,
  `categories` VARCHAR(45) NULL,
  `tags` VARCHAR(45) NULL,
  `image` VARCHAR(150) NULL,
  `pricing_excluded_price` BIGINT(20) NULL,
  `pricing_included_price` BIGINT(20) NULL,
  `pricing_tax_rate` BIGINT(20) NULL,
  `pricing_compared_price` BIGINT(20) NULL,
  `inventory_sku` BIGINT(20) NULL,
  `inventory_quantity` BIGINT(20) NULL,
  `shipping_width` BIGINT(20) NULL,
  `shipping_height` BIGINT(20) NULL,
  `shipping_weight` BIGINT(20) NULL,
  `shipping_tax_rate` BIGINT(20) NULL,
  `extra_shipping_rate` BIGINT(20) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
