ALTER TABLE `user_info` 
DROP COLUMN `gmail_id`,
DROP COLUMN `fb_id`;

ALTER TABLE `login_info` 
ADD COLUMN `random_pwd` VARCHAR(255) NULL;